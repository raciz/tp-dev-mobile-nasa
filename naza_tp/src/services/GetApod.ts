import { _NazaApodList } from './../types/NazaApod';
import axios from 'axios'

/*
* function for request Nasa API -> /planetary/apod
* return apod data or null
*/
export const getNasaPictures = async (date: Date) => {
  let nasaResponse = null;
  const apiKey = "IzwcogsoiWdMOc4gXed1fMmFhhd9oFrBCb9i35im"; // api key normaly set in process.env

  const formatedDate = `${date.getFullYear()}-${date.getMonth()}-${date.getDate()}`; // date formating for nasa api

  try {
    nasaResponse = await axios.get(`https://api.nasa.gov/planetary/apod?api_key=${apiKey}&date=${formatedDate}`);
    if (nasaResponse?.data) return nasaResponse.data;
  } catch (err) { console.log("ERROR: ", err) }
  return null
}

