

// Nasa api response type for get apod
export interface _NazaApod {
    copyright?: string;
    date: string;
    explanation: string;
    hdurl: string;
    media_type: string;
    service_version: string;
    title: string;
    url: string;
}

// apod list
export type _NazaApodList = Array<_NazaApod>