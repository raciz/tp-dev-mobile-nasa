// React
import React, { useEffect, useState } from "react";
import {
  ActivityIndicator,
  StyleSheet,
  Text,
  View,
  ScrollView,
  Image,
  TouchableOpacity,
} from "react-native";

//Types
import { _NazaApod, _NazaApodList } from "../types/NazaApod";

type Props = {
    setSelected: (selected: _NazaApod) => void
    nasaPictures: _NazaApodList
    loading: boolean
};

/*
* PicturesList components
* @setSelected
* @nasaPictures
* @loading
*/
const PicturesList: React.FC<Props> = ({setSelected, nasaPictures, loading}) => {
    const loadingTab = []

    if (loading) for (let i = 0; i < 10; i++) loadingTab.push(i) // tmp loading list for display 10 loaders

    // useeffect for control component loading
    useEffect(() => {
        console.log("LIST LOAD WITH ", nasaPictures.length, "ELEMENTS");
        return () => {};
      }, []);

  return (
    <>
      <View style={styles.container}>
        <ScrollView horizontal style={styles.scrollView}>
          {loading === false ? nasaPictures.map((element) => (
            <TouchableOpacity style={styles.button} key={nasaPictures.indexOf(element)} onPress={() => {setSelected(element)}}>
              <Image
                style={styles.image}
                source={{
                  uri: element.url,
                  width: 68,
                  height: 68,
                }}
              />
            </TouchableOpacity>
          )):
          loadingTab.map((element) => (
            <TouchableOpacity style={styles.button} key={element}>
              <ActivityIndicator size={68} style={styles.image} color="#ffffff" />
            </TouchableOpacity>
          ))
          }
        </ScrollView>
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    height: 90,
    backgroundColor: "#000000",
    marginBottom: 25,
    marginTop: 26
  },
  scrollView: {
    marginLeft: 20,
  },
  button: {
    padding: 10,
  },
  image: {
    borderRadius: 17,
    borderColor: "#453E5A",
    borderWidth: 3,
  },
});

export default PicturesList;
