import { StyleSheet, Platform } from 'react-native';

/*
* - android safe area for notification bar -> droidSafeArea
*
*
*/

export default StyleSheet.create({
    droidSafeArea: {
        flex: 1,
        paddingTop: Platform.OS === 'android' ? 35 : 0
    },
});

